from flask import Flask
from flask import render_template, request, redirect, url_for
import json

app = Flask(__name__)

customers = [{'id': 1, 'name': 'danny', 'address': 'tel-aviv'},
             {'id': 2, 'name': 'suzi', 'address': 'california'},
             {'id': 3, 'name': 'rubi', 'address': 'tokyo'}]

@app.route('/customers', methods = ['GET','POST'])
def getpostcustomer():
    if request.method == 'GET':
        return json.dumps(customers)
    if request.method == 'POST':
        new_customer = request.get_json()
        print(new_customer)
        customers.append(new_customer)
        return json.dumps(customers)

@app.route('/customers/<int:id>', methods=['GET', 'PUT', 'DELETE'])
def putdeletecustomer(id):
    if request.method == 'DELETE':
        result = [c for c in customers if c["id"] != id]
        return json.dumps(result)
    if request.method == 'PUT':
        updated_customer = request.get_json()
        print(updated_customer)
        for c in customers:
            if c["id"] == id:
                c["id"] = updated_customer["id"]
                c["name"] = updated_customer["name"]
                c["address"] = updated_customer["address"]
        return json.dumps(customers)
    if request.methon == 'GET':
        for c in customers:
            if c["id"] == id:
                return json.dumps(c)
        return '{}'

app.run()

# REST API orders
# 1 create list of orders with json objects [dictionary]
#       order: ID, ITEM-ID, ITEM-PRICE, QUANTITY, TOTAL-PRICE
#                   TOTAL-PRICE = ITEM-PRICE x QUANTITY
# 2 build 2 methods for GET POST PUT/ID DELETE/ID GET/ID
#   POST - { id : .. , item-id : .. , item-price : .. , quantity : ... }
#       before adding , calc the total-price as quantity x item-price
#   PUT - before updating ... update total-price [ignore the total-price in json]
# 0 total_orders_sum
# *etgar: total_sum should be updated upon post , put, delete
# GET/0 --> return total_sum



